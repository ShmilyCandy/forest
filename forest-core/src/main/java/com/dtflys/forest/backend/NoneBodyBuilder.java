package com.dtflys.forest.backend;

import com.dtflys.forest.http.ForestRequest;

/**
 * @author gongjun[jun.gong@thebeastshop.com]
 * @since 2017-05-19 15:11
 */
public class NoneBodyBuilder implements BodyBuilder {
    @Override
    public void buildBody(Object req, ForestRequest request) {
}
}
